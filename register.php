<!DOCTYPE html>
<html>
    <head>
        <title>myPageRegister</title>
        <link rel="stylesheet" type="text/css" href="style.css">
        <script type="text/javascript">
            function validate() {
                if(document.myForm.NAME.value==""){
                    alert("Please provide your Name!!");
                    document.myForm.NAME.focus();
                    return false;
                }
                if(document.myForm.PHONENO.value==""){
                    alert("Please provide your Phone Number!!");
                    document.myForm.PHONENO.focus();
                    return false;
                }
                if(document.myForm.EMAIL.value==""){
                    alert("Please provide your EMail Id!!");
                    document.myForm.EMAIL.focus();
                    return false;
                }
                if(document.myForm.PASSWORD.value==""){
                    alert("Please provide your Password!!");
                    document.myForm.PASSWORD.focus();
                    return false;
                }
                if(PASSWORD.length<8){
                    alert("Password must be atleast 8characters long!!");
                    document.myForm.PASSWORD.focus();
                    return false;
                }
                
                if(phone.length!=10){
                    alert("Please enter 10 digit Phone Number Only!!")
                }
                    var emailID = document.myForm.EMAIL.value;
                    atpos = emailID.indexOf("@");
                    dotpos = emailID.lastIndexOf(".");
                    if (atpos < 1 || ( dotpos - atpos < 2 )) {
                        alert("Please enter correct email ID")
                        document.myForm.EMail.focus() ;
                        return false;
                    }
                    return( true );
                }
        </script>
    </head>
    <body>
        <header>
            <div class="boxr">
                <h1>Sign Up Here</h1>
                <form action="registration.php" method="post" onsubmit="return(validate());" >
                    <table class="form_table">
                        <tr class="form_table_row">
                            <td class="form_table_column1"><h2>Name:</h2></td>
                            <td class="form_table_column2"><h2><input type="text" name="NAME" placeholder="Enter Name"></h2></td>
                        </tr>
                        <tr class="form_table_row">
                            <td class="form_table_column1"><h2>Phone Number:</h2></td>
                            <td class="form_table_column2"><h2><input type="number" name="PHONENO" id="phone" placeholder="Enter Phone Number"></h2></td>
                        </tr>
                        <tr class="form_table_row">
                            <td class="form_table_column1"><h2>E-Mail:</h2></td>
                            <td class="form_table_column2"><h2><input type="email" name="EMAIL" placeholder="Enter Email ID"></h2></td>
                        </tr>
                        <tr class="form_table_row">
                            <td class="form_table_column1"><h2>Password:</h2></td>
                            <td class="form_table_column2"><h2><input type="password" name="PASSWORD" placeholder="Enter Password"></h2></td>
                        </tr>
                        <tr class="form_table_row">
                            <td class="form_table_column1"><h2>Unique Code:</h2></td>
                            <td class="form_table_column2"><h2><input type="number" name="CODE" placeholder="Enter Unique Code"></h2></td>
                        </tr>
                        <tr class="form_table_row">
                            <td class="form_table_column1">
                                <input type="submit" name="" value="Register"><t>
                            </td>
                        </tr>
                        <tr>
                            <td class="form_table_column2"><a href="login.php">Back</a><br></td>
                        </tr>
                    </table>    
                </form>
            </div>
        </header>
    </body>
</html>

<!--onsubmit="return(validate());"-->